<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Registration Confirmation</title>
	</head>
	<body>
		<%
			String discovery = session.getAttribute("discovery").toString();
			if(discovery.equals("friends")){
				discovery = "Friends";
			}
			else if(discovery.equals("socialmedia")){
				discovery = "Social Media";
			}
			else{
				discovery = "Others";
			}
			
			String dateOfBirth = session.getAttribute("dateOfBirth").toString();
		%>
		
		<h1>Registration Confirmation</h1>
		<p>First Name: <%= session.getAttribute("firstname") %></p>
		<p>Last Name: <%= session.getAttribute("lastname") %></p>
		<p>Phone Number: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= discovery %></p>
		<p>Date of Birth: <%= dateOfBirth %></p>
		<p>User Type: <%= session.getAttribute("employerApplicant") %></p>
		<p>Description: <%= session.getAttribute("profile") %></p>
		
		<!-- Submit button for booking -->
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<!-- Button to go back at index.jsp -->
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
		
	</body>
</html>
