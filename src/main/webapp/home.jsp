<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Welcome <%= session.getAttribute("firstname") %> <%= session.getAttribute("lastname") %>!</title>
	</head>
	<body>
		<h1>Welcome <%= session.getAttribute("firstname") %> <%= session.getAttribute("lastname") %>!</h1>
		<p>
		<%
			String employerApplicant = session.getAttribute("employerApplicant").toString();
			if(employerApplicant.equals("employer")){
				employerApplicant = "employer";
			}
			else if(employerApplicant.equals("applicant")){
				employerApplicant = "applicant";
			}
		%>
		
		<span data-show-if="employerApplicant == 'employer'">
		Welcome employer. You may now start browsing applicant profiles.
		</span>
		<span data-show-if="employerApplicant = 'applicant'">
		Welcome applicant. You may now start looking for your career opportunity.
		</span>
		</p>
	</body>
</html>
