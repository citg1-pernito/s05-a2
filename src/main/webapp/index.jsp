<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Welcome to Servlet Job Finder!</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			
			div > input, div > select, div > textarea{
				width: 96%;
			}
			
		</style>
	</head>
	<body>
			
		<div id="container">
		
		<h1>Welcome to Servlet Job Finder!</h1>
		
			<form action="register" method="post">
				<!-- First Name -->
				<div>
					<label for="firstname">First Name</label>
					<input type="text" name="firstname" required>
				</div>
				
				<!-- Last Name -->
				<div>
					<label for="lastname">Last Name</label>
					<input type="text" name="lastname" required>
				</div>
				
				<!-- Phone Number -->
				<div>
					<label for="phone">Phone Number</label>
					<input type="tel" name="phone" required>
				</div>
				
				<!-- Email Address -->
				<div>
					<label for="email">Email Address</label>
					<input type="email" name="email" required>
				</div>
				
				<!-- App discovery choice -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<!-- Friends option -->
					<input type="radio" id="friends" name="discovery" value="friends" required>
					<label for="friends">Friends</label>
					
					<!-- Social Media option -->
					<input type="radio" id="social_media" name="discovery" value="socialmedia" required>
					<label for="social_media">Social Media</label>
					
					<!-- Others option -->
					<input type="radio" id="others" name="discovery" value="others" required>
					<label for="others">Others</label>
				</fieldset>
				
				<!-- Extras -->
				<!-- <fieldset>
					<legend>Extras</legend>
					
					Baby Seat
					<input type="checkbox" name="extras_baby" value="baby_seat">
					<label for="extras_baby">Baby Seat</label>
					
					Wheel Chair
					<input type="checkbox" name="extras_wheelchair" value="wheelchair_assistance">
					<label for="extras_wheelchair">Wheelchair Assistance</label>
				</fieldset> -->
				
				<!-- Date of birth -->
				<div>
					<label for="date_of_birth">Date of birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
				<!-- Employer / applicant choices -->
				<div>
					<label for="employer_applicant">Are you an employer or an applicant?</label>
					<select id="emp_app" name="employer_applicant">
						<option value="" selected="selected">-- Please choose another option --</option>
						<option value="employer">Employer</option>
						<option value="applicant">Applicant</option>
					</select>
				</div>
				
				<!-- Dropoff Place -->
				<!-- <div>
					<label for="destination">Dropoff Place</label>
					<input type="text" name="destination" required list="destinations">
					datalist provide autocomplete feature
					<datalist id="destinations">
						<option value="Home">
						<option value="Office">
						<option value="Airport">
						<option value="Beach">
					</datalist>
				</div> -->
				
				<!-- Profile Description -->
				<div>
					<label for=profile>Profile Description</label>
					<textarea name="profile" maxlength="500"></textarea>
				</div>
				
				<!-- Button for Submit -->
				<button>Submit Booking</button>
			</form>
		</div>
	</body>
</html>
