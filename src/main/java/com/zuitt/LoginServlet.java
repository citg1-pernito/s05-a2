package com.zuitt;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	ArrayList<String> info = new ArrayList<>();
	private int infoCount;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286177122798244808L;

	public void init() throws ServletException{
		System.out.println("LoginServlet has been initialized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String date = LocalTime.now().toString();
		
		infoCount++;
		
		String orderNumber = infoCount + date.replaceAll("[^a-zA-Z0-9]+", "");
		
		info.add(orderNumber);
		
		HttpSession session = req.getSession();
        session.setAttribute("info", info);
        
        res.sendRedirect("home.jsp");
	}
	
	public void destroy(){
		System.out.println("LoginServlet has been finalized");
	}
}

